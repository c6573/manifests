# Manifests

In de `.gitlab` directory zit de config van de agent die in een kubernetes-cluster is geinstalleerd.
In dit geval eentje op Google Cloud Platform.  

Alle projecten die in de dezelfde gitlab-groep zitten als waar dit project in zit, kunnen gebruik maken van deze agent.
Hierdoor hoeven we geen credentials meer uit te wisselen, maar gebruiken we feitelijk een tunnelverbinding naar het cluster.

Belangrijk is om eerst de juiste context te selecteren via `kubectl config use-context`